package com.joe.module.api.response.getAlbums;

import com.joe.module.api.request.createRequest.CreateAlbumRequest;
import lombok.Data;

@Data
public class GetAlbumResponse {
    private String id;
    private String market;
    private CreateAlbumRequest createAlbumRequest;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMarket() {
        return market;
    }

    public void setMarket(String market) {
        this.market = market;
    }

    public CreateAlbumRequest getCreateAlbumRequest() {
        return createAlbumRequest;
    }

    public void setCreateAlbumRequest(CreateAlbumRequest createAlbumRequest) {
        this.createAlbumRequest = createAlbumRequest;
    }
}
