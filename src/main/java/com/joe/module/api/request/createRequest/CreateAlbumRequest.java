package com.joe.module.api.request.createRequest;

import lombok.Data;

@Data
public class CreateAlbumRequest {
    private String id;
    private String market;

    public void setId(String name) {
        this.id=id;
    }

    public void setMarket(String market) {
        this.market=market;
    }

    public String getId() {
        return id;
    }

    public String getMarket() {
        return market;
    }
}
