package com.joe.module.api.data;

import io.restassured.response.Response;

public class UsersData {
    
    private static String id;
    private static String market;
    private static Response getAlbumResponse;

    public static String  getId() {
        return id;
    }

    public static String  getMarket() {
        return market;
    }

    public static void setId(String id) {
        UsersData.id = id;
    }

    public static void setMarket(String market) {
        UsersData.market = market;
    }

    public static Response getGetAlbumResponse() {
        return getAlbumResponse;
    }

    public static void setGetAlbumResponse(Response getAlbumResponse) {
        UsersData.getAlbumResponse = getAlbumResponse;
    }

}
