package com.joe.module.api;

import com.joe.module.api.data.UsersData;
import com.joe.module.api.request.createRequest.CreateAlbumRequest;
import io.restassured.response.Response;

import static net.serenitybdd.rest.SerenityRest.given;

public class UsersController {
    public static Response getUser () {
        return  given().
                    header("Accept", "application/json").
                when().
                    get("https://api.spotify.com/v1/albums/" + UsersData.getId()+"?market="+UsersData.getMarket());
    }

    /*public static Response createUser() {
        CreateUserRequest request = new CreateUserRequest();
        request.setName(UsersData.getName());
        request.setJob(UsersData.getJob());
        return  given().
                    header("Content-Type", "application/json").
                    header("Accept", "application/json").
                    body(request).
                when().
                    post("https://reqres.in/api/users");
    }*/

    public static Response createAlbum() {
        CreateAlbumRequest request = new CreateAlbumRequest();
        request.setId(UsersData.getId());
        request.setMarket(UsersData.getMarket());
        return  given().log().all().auth().oauth2("BQB0-DN4C7f6caJmRQ_5uG2b-Fbvnvwlm9p5tyqYqB1-i2Z3YXXyTVxwm_daoVsb90WW3fV08jifUWrrIddbnAilk4rJRS-I1z7tLAVKuTFU2twzOKqh3Ca5rcTKaK1WpMqV7CDR9tuvIzUQZVDEjH3dCqWH2XAI1Epj").
                header("Content-Type", "application/json").
                header("Accept", "application/json").
                //header("Authorization", "").
                //header("Authorization").auth().oauth2("BQCo7af8306wCc3ARnkMTJuZNvwpHlWUJ-nAbBV5LoJK8MTe2alre-R2HZl03cinQswPKzgL_jDN7U403rwTevZVkAJFPkYXpfsON0M9-eyhkSwky-h-ACHUjfDeA2-usgXsL-ESKBGf5j8197rxMA7gLd9-f1nF7c94").
               // header("Authorization","BQCo7af8306wCc3ARnkMTJuZNvwpHlWUJ-nAbBV5LoJK8MTe2alre-R2HZl03cinQswPKzgL_jDN7U403rwTevZVkAJFPkYXpfsON0M9-eyhkSwky-h-ACHUjfDeA2-usgXsL-ESKBGf5j8197rxMA7gLd9-f1nF7c94").
                //body(request).
                when().
                        get("https://api.spotify.com/v1/albums/" + UsersData.getId()+"?market="+UsersData.getMarket()).then().log().all().extract().response();
    }

}