@CreateUserFeature @Users @API @Regression
Feature: Get albums

  @Positive
  Scenario: Get Album
    Given [api] set album id to '4aawyAB9vmqN3uQ7FjRGTy'
    And   [api] set market to 'ES'
    When  [api] send get Album request
    Then  [api] get single user status code must be '200'
    And   [api] create user response equals with request

  @Negative
  Scenario: Get Album
    Given [api] set album id to ''
    And   [api] set market to 'ES'
    When  [api] send get Album request
    Then  [api] get single user status code must be '401'
    And   [api] create user response equals with request